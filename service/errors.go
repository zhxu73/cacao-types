// Package service this file contains general error strings, types, or constants that are used within the service package
package service

// these general errors strings, perhaps applicable to multiple service
// to create a new error object from this string, call error.New(string)
const (
	GeneralUnmarshalFromUserMSError string = "could not unmarshal User object on User microservice side"
	GeneralEventOpTimeoutError      string = "event operation timeout"
	GeneralEventOpCanceledError     string = "event operation was canceled"
	GeneralActorNotAuthorizedError  string = "actor not authorized"
	GeneralActorNotFoundError       string = "actor was not found; perhaps you can use the bypass option"
	GeneralMethodNotYetImplemented  string = "method not yet implemented"
)

// these are user object type of errors
// to create a new error, call error.New(string)
const (
	UserUsernameNotSetError     string = "username was not set before using User" // if username not initialized before Load
	UserActorNotSetError        string = "actor was not set"
	UserUsernameNotFoundError   string = "username not found"
	UserUsernameExistsCannotAdd string = "username exists, cannot add user"
	UserUpdateError             string = "user could not be updated"
	UserDeleteError             string = "user could not be deleted"
)

// these are user list types of errors
// to create a new error, call error.New(string)
const (
	UserListFilterNotSetError            string = "user list filter was set before Search/SearchNext"
	UserListFilterInvalidStartIndexError string = "user list filter has invalid start index"
	UserListFilterInvalidMaxItemsError   string = "user list filter has invalid max items"
	UserListLoadBoundaryError            string = "next start index is invalid or no more elements to load beyond end of user list"
	UserListEmptyListError               string = "user list results size 0"
)

// these are just warning strings
// to create a new error, call error.New(string)
const (
	UserListFilterValueEmptyWarning string = "Warning UserFilter value was not set, will return all"
)
