The standard message format for CACAO is the [CloudEvent](https://cloudevents.io/). The following is an example a generic CloudEvent in CACAO for a mythical microservice:

```
{
    "specversion": "1.0",
    "type": "org.cyverse.events.MythicalCreated",
    "source": "https://gitlab.com/cyverse/mythical-microservice/mythical-85f9bbcd97-kttdg",
    "id": "cloudevent-c0usj6qljdkc1t9civvg",
    "time": "2020-02-28T12:13:39.4589254Z",
    "datacontenttype": "application/json; charset=utf-8",
    "data": {
        ...
    }
}
```

# Attributes

#### specversion

`specversion` will currently always be `"1.0"`

#### type

`type` will contain the message type as related to the query operation or event. [Per the CloudEvent spec](https://github.com/cloudevents/spec/blob/master/spec.md#type), this value is prefixed with reverse dns name starting with "org.cyverse" and directly maps to query channels and events. Examples include:

    "org.cyverse.mythical.Get"
    "org.cyverse.events.MythicalDeleted"
    "org.cyverse.workspaces.List"
    "org.cyverse.events.WorkspaceCreated"
    "org.cyverse.users.Get"
    "org.cyverse.users.List"
    "org.cyverse.events.UserUpdated"
### source

`source` will contain the URI for the (micro)service that created the message. If relevant, then the Pod Name should also appended at the end of the URL following the microservice name. Examples include

    https://gitlab.com/cyverse/mythical-microservice/{POD_NAME}
    https://gitlab.com/cyverse/users-microservice/{POD_NAME}
    https://gitlab.com/cyverse/wiretap-microservice/{POD_NAME}
    https://gitlab.com/cyverse/nafigos/api-service/{POD_NAME}
    https://gitlab.com/cyverse/nafigos/workspace-service/{POD_NAME}
    https://gitlab.com/cyverse/nafigos/credential-service/{POD_NAME}

### id

`id` will contain "cloudevent-" concatenated with an xid. Examples include:

    cloudevent-9m4e2mr0ui3e8a215n4g

### time

`time` will contain the message creation timestamp in ISO 8601 format in UTC timezone. In golang, this is can be generated with the following code:

    time.Now().UTC().Format(time.RFC3339)

An example of this format:

    2021-03-02T05:11:12Z

### datacontenttype

`datacontenttype` will always be `"application/json; charset=utf-8"`

### data

`data` will contain the message json message body according to `type`.
